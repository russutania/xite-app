Xite app\
● Retrieve a JSON file via a http request which contains a dataset of music videos we’ve prepared for you: 
https://raw.githubusercontent.com/XiteTV/frontend-coding-exercise/main/data/dataset.json \
● The genre_id value of a music video object needs to be mapped to a specific label\
● The app should show the data it has retrieved if the http request was successful\
● It should be possible to search through and/or filter the retrieved data\
● The app needs to be responsive.


In the project directory, you can run:

### `npm install`
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


-> Used Material UI for UI.\
-> Used fetch API for retrieving data from JSON file via a http request (alternative: axios, async/await).\
-> You can search by artist name or video title, filter by genre (multiple select) and filter by year.\
Note: for filter year select I mapped the years which appears at least once in the videos array.\
-> Used Grid for responsive (alternative: flexbox, media queries).