import React from 'react'
import TextField from "@material-ui/core/TextField";
import Autocomplete from '@material-ui/lab/Autocomplete';
import {withStyles} from "@material-ui/core/styles";

const styles = () => ({
  tagStyle: {
    margin: "0 2px",
    height: "27px"
  },
})

const AutocompleteFilter = ({ id, multiple, options, getOptionLabel, getOptionSelected, placeholder, onChange, classes }) => {
  return (
    <Autocomplete
      multiple={multiple}
      id={id}
      classes={{
        tag: classes.tagStyle
      }}
      options={options}
      getOptionLabel={getOptionLabel}
      onChange={onChange}
      getOptionSelected={getOptionSelected}
      renderInput={(params) => (
        <TextField
          {...params}
          size="small"
          variant="outlined"
          placeholder={placeholder}
        />
      )}
    />
  )
};


export default withStyles(styles)(AutocompleteFilter)


