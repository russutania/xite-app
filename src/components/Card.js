import React from 'react'
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import { withStyles } from '@material-ui/core/styles';

const styles = () => ({
  media: {
    height: 0,
    paddingTop: "56.25%",
  },
  title: {
    fontSize: "14px",
    fontWeight: "bold"
  },
  subheader: {
    fontSize: "14px"
  }
});

const MusicCard = ({ image, artist, title, classes }) => {
  return (
    <Card className={classes.root}>
      <CardMedia
        className={classes.media}
        image={image}
      />
      <CardHeader
        title={artist}
        subheader={title}
        classes={{
          title: classes.title,
          subheader: classes.subheader
        }}
      />
    </Card>
  )
};


export default withStyles(styles)(MusicCard)


