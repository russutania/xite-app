import React, { Component } from 'react'
import MusicCard from "../components/Card";
import AutocompleteFilter from "../components/Autocomplete";
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import { withStyles } from "@material-ui/core/styles";

const styles = () => ({
  wrapper: {
    padding: "0px 15px",
    width: "100%",
    margin: 0
  },
  search: {
    width: '100%'
  },
  input: {
    height: "40px"
  },
  noData: {
    margin: 0,
    paddingLeft: "15px",
    color: "#7e817e",
    fontSize: "16px"
  }
})

class MusicBoard extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      genreList: [],
      isLoaded: false,
      error: null,
      filterTitleOrArtist: '',
      filterYear: '',
      filterGenre: [],
      filteredResult: []
    }
  }

  componentDidMount() {
    fetch('https://raw.githubusercontent.com/XiteTV/frontend-coding-exercise/main/data/dataset.json')
      .then(res => res.json())
      .then((data) => {
          this.setState({
            isLoaded: true,
            data: data["videos"],
            genreList: data["genres"]
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    const { data, filterYear, filterGenre, filterTitleOrArtist } = this.state
    if(filterYear !== prevState.filterYear || filterGenre !== prevState.filterGenre || filterTitleOrArtist !== prevState.filterTitleOrArtist) {
      const filters = {};
      if (filterYear) {
        filters.release_year = (release_year) => filterYear === release_year
      }
      if (filterGenre.length) {
        filters.genre_id = (genre_id) => filterGenre.includes(genre_id)
      }
      if (filterTitleOrArtist) {
        filters.searchBox = (artist, title) => artist.toString().toLocaleLowerCase().includes(filterTitleOrArtist) || title.toString().toLocaleLowerCase().includes(filterTitleOrArtist)
      }

      const filteredVideos = this.filterData(data, filters)

      this.setState({
        filteredResult: filteredVideos
      })
    }
  }

  filterData = (data, filters) => {
    const filterKeys = Object.keys(filters);
    return data.filter(video => {
      return filterKeys.every(key => {
        if(key === "searchBox") {
          return filters[key](video["artist"], video["title"])
        } else {
          return filters[key](video[key])
        }
      });
    });
  };

  handleChangeGenres = (event, value) => {
    let selectedGenreArray = [];
    value.forEach((el) => {
      selectedGenreArray.push(el.id)
    });

    this.setState({
      filterGenre: selectedGenreArray
    })
  };

  handleChangeYear = (event, value) => {
    this.setState({
      filterYear: value ? value.id : ""
    })
  }

  handleChangeName = (event) => {
    this.setState({
      filterTitleOrArtist: event.target.value
    })
  }

  render() {
    const { data, genreList, error, isLoaded, filteredResult, filterYear, filterGenre, filterTitleOrArtist} = this.state;
    const { classes } = this.props;
    const yearArray = [];

    const finalData = filteredResult.length || (filterYear || filterTitleOrArtist|| filterGenre.length) ? filteredResult : data;

    data && data.map(video => yearArray.push(video.release_year));

    const sortedYearArray = [...new Set(yearArray)].sort((a,b) => a-b);

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <Grid container spacing={3} className={classes.wrapper}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <TextField
              id="titleOrArtistFilter"
              variant="outlined"
              className={classes.search}
              InputProps={{
                className: classes.input,
                endAdornment: (
                  <InputAdornment>
                    <SearchIcon/>
                  </InputAdornment>
                )
              }}
              placeholder="Search title or artist"
              value={filterTitleOrArtist}
              onChange={this.handleChangeName}
            />
          </Grid>

          <Grid item xs={12} sm={6} md={6} lg={6}>
            <AutocompleteFilter
              id="genreFilter"
              multiple
              options={genreList}
              getOptionLabel={(option) => option.name}
              onChange={((event, value) => this.handleChangeGenres(event, value))}
              placeholder="Select genre"
            />
          </Grid>

          <Grid item xs={12} sm={6} md={6} lg={6}>
            <AutocompleteFilter
              id="yearFilter"
              options={sortedYearArray.map((el)=> {return {id: el, name: `${el}`}})}
              getOptionLabel={(option) => option.name}
              getOptionSelected={(option, value) => option.id === value.id}
              onChange={((event, value) => this.handleChangeYear(event, value))}
              placeholder="Select year"
            />
          </Grid>

          {finalData.length ?
            finalData.map(video => (
              <Grid item key={video.id} xs={12} sm={6} md={4} lg={3}>
                <MusicCard
                  image={video["image_url"]}
                  artist={video.artist}
                  title={video.title}
                />
              </Grid>
            )) :
            <h3 className={classes.noData}>No data to display</h3>
          }
        </Grid>
      )
    }
  }
}

export default  withStyles(styles)(MusicBoard)