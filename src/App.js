import React from 'react'
import './App.css';
import MusicBoard from "./containers/MusicBoard";

function App() {
  return (
    <div className="App">
      <MusicBoard/>
    </div>
  );
}

export default App;
